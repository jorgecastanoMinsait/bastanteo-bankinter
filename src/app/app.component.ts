import { Component } from '@angular/core';

import { faAngleDown } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bastanteo-bankinter';
  faAngleDown = faAngleDown;
}
