import { TestBed } from '@angular/core/testing';

import { ListaTareasService } from './lista-tareas.service';

describe('ListaTareasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListaTareasService = TestBed.get(ListaTareasService);
    expect(service).toBeTruthy();
  });
});
