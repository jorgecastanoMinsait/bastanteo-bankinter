import { Tarea } from '../../../core/models/tarea';

export const TAREAS: Tarea[] = [{
  estado: 'Nueva solicitud',
  id: '5555',
  name: 'name1',
  cif: '12368Z',
  clave_oficina: '1',
  id_usuario: 'AAA',
  observaciones: 'obs1',
  fecha_solicitud: '11/03/2018',
  prioridad: '1',
  numero_solicitud: '123',
  idTarea: 'idTarea1'
}, {
  estado: 'Nueva solicitud',
  id: '5556',
  name: 'name2',
  cif: '15678Z',
  clave_oficina: '22',
  id_usuario: 'AAA',
  observaciones: 'obs2',
  fecha_solicitud: '11/03/2018',
  prioridad: '1',
  numero_solicitud: '123',
  idTarea: 'idTarea2'
}, {
  estado: 'Nueva solicitud',
  id: '5557',
  name: 'name3',
  cif: '12315678Z',
  clave_oficina: '22',
  id_usuario: 'AAA',
  observaciones: 'obs3',
  fecha_solicitud: '11/03/2018',
  prioridad: '1',
  numero_solicitud: '123',
  idTarea: 'idTarea3'
}];
