import { Component, OnInit } from '@angular/core';

import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { ListaTareasService } from './lista-tareas.service';

import { TAREAS } from './mocks/listaTareas';

@Component({
  selector: 'app-lista-tareas',
  providers: [ ListaTareasService ],
  templateUrl: './lista-tareas.component.html',
  styleUrls: ['./lista-tareas.component.scss']
})
export class ListaTareasComponent implements OnInit {

  loading = false;
  tasks = [];
  showActions = null;

  faSearch = faSearch;
  searchText = '';

  constructor(private tareasService: ListaTareasService) { }

  ngOnInit() {
    // this.tasks = TAREAS;
    this.loading = true;
    this.tareasService.getTareas()
      .subscribe(tareas => {
        this.loading = false;
        console.log(tareas);
        this.tasks = TAREAS;
      });

    //     // OLD ---- Recuperamos las variables de esa tarea
    //     var tarea = {
    //         estado: 'Nueva solicitud',
    //         id: tareas[i].processInstanceId,
    //         name: tareas[i].name,
    //         cif: $filter('filter')(tareas[i].variables, {'name':'cif'})[0].value,
    //         clave_oficina: $filter('filter')(tareas[i].variables, {'name':'clave_oficina'})[0].value,
    //         id_usuario: $filter('filter')(tareas[i].variables, {'name':'id_usuario'})[0].value,
    //         observaciones: $filter('filter')(tareas[i].variables, {'name':'observaciones'})[0].value,
    //         fecha_solicitud: $filter('filter')(tareas[i].variables, {'name':'fecha_solicitud'})[0].value,
    //         prioridad: $filter('filter')(tareas[i].variables, {'name':'prioridad'})[0].value,
    //         numero_solicitud: $filter('filter')(tareas[i].variables, {'name':'numero_solicitud'})[0].value,
    //         idTarea: tareas[i].id
    //     }
  }

  clearFilter() {
    if (this.searchText !== '') {
      this.searchText = '';
    }
  }

  createNewTask() {
    console.log('Nueva tarea');
  }

}
