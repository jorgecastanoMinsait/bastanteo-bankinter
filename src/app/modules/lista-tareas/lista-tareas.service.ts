import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';


@Injectable()
export class ListaTareasService {

  private tareasUrl = environment.url + '/ws/listarTareas';

  constructor(private http: HttpClient) { }

  getTareas() {
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/form-data'
    //     // 'Content-Type': 'application/json'
    //   })
    // };

    const formData: FormData = new FormData();
    formData.append('userId', 'admin');

    return this.http.post(this.tareasUrl, formData).pipe(
        tap(_ => console.log('getTareas OK')),
        catchError(this.handleError('getTareas'))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<HttpErrorCode>(operation = 'operation', result?: HttpErrorCode) {
    return (error: any): Observable<HttpErrorCode> => {
      console.error(operation, error.status + ' ' + error.statusText); // log to console
      // Let the app keep running by returning an empty result.
      return of(error as HttpErrorCode);
    };
  }
}
