import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListaTareasComponent } from './modules/lista-tareas/lista-tareas.component';

import { AppRoutingModule } from './app-routing.module';

import { SearchTaskPipe } from './core/pipes/search-task.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ListaTareasComponent,

    SearchTaskPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
