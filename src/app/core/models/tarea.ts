export class Tarea {
  estado: string;
  id: string;
  name: string;
  cif: string;
  clave_oficina: string;
  id_usuario: string;
  observaciones: string;
  fecha_solicitud: string;
  prioridad: string;
  numero_solicitud: string;
  idTarea: string;
}
