import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchTask'
})

export class SearchTaskPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }

    if (!searchText) {
      return items;
    }

    searchText = searchText.toLowerCase();

    return items.filter( it => {
      for (const key in it) {
        if (it[key] && it[key].toLowerCase().includes(searchText)) {
          return true;
        }
      }
      return false;
    });
  }
}
