import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import { SharedModule } from '@app/shared/shared.module';
import { ListaTareasComponent } from './modules/lista-tareas/lista-tareas.component';

const routes: Routes = [
  { path: '', redirectTo: '/inbox', pathMatch: 'full' },
  { path: 'inbox', component: ListaTareasComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes), /* SharedModule */ ],
  exports: [ RouterModule ]
})
export class AppRoutingModule  { }
